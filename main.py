#Bibliotecas e configurações do selenium
from flask import Flask, render_template, url_for, request, redirect
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service 
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time 
import pandas as pd 
import os
chrome_options = Options()
chrome_options.add_experimental_option("detach",True)
server = Service(ChromeDriverManager().install())

#Variavéis globais
navegador = None 
verifica = None
escolha_livro = 0


app = Flask(__name__)



def tentar():
    #Tenta pegar o titulo da pagina, caso não consiga repete o processo até conseguir
    try:
        verifica = navegador.find_element(By.CLASS_NAME,"_p13n-zg-banner-list-page-header_style_zgListPageBannerText__UU0H0")
    except:
        time.sleep(0.5)
        navegador.refresh()

def criar_pasta():
    #Verifica se a pasta arquivos existe, se não existir cria ela
    if not os.path.exists("Arquivos"):  
        os.makedirs("Arquivos")
    else:
        pass


#rotas
@app.route('/', methods=['GET','POST'])
def index(): 
    #Dados da página index, Onde cada botão envia um valor captado pelo escolha livro. Dependendo do valor ele seleciona uma página para realizar 
    #a extração dos dados da pagina da Amazon
    global navegador 
    escolha_livro = 0 
    escolha_livro = request.form.get("botao")
    print("O valor do botao é:",escolha_livro)
    if escolha_livro == "1":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/ref=zg_bs_unv_books_1_7872854011_1")
        tentar()
        return redirect(url_for('livros')) 
    
    elif escolha_livro == "2":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7872854011/ref=zg_bs_nav_books_1") #ADM
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "3":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7841296011/ref=zg_bs_nav_books_1") #Artes
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "4":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7841521011/ref=zg_bs_nav_books_1")#Artesanato
        tentar()
        return redirect(url_for('livros'))

    elif escolha_livro == "5":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7841720011/ref=zg_bs_nav_books_1") #AutoAjuda
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "6":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7841731011/ref=zg_bs_nav_books_1") #Biografias
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro== "7":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7841795011/ref=zg_bs_nav_books_1") #Ciências
        tentar()
        return redirect(url_for('livros'))

    elif escolha_livro == "8":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7842641011/ref=zg_bs_nav_books_1") #Computação 
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "9":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7843925011/ref=zg_bs_nav_books_1") #Crônicas 
        tentar()
        return redirect(url_for('livros'))

    elif escolha_livro  == "10":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7874340011/ref=zg_bs_nav_books_1")#Direito
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "11":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7842837011/ref=zg_bs_nav_books_1")#Educação 
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "12":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/12764254011/ref=zg_bs_nav_books_1") #Engenharia
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "13":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7843058011/ref=zg_bs_nav_books_1") #Erótico
        tentar()
        return redirect(url_for('livros'))
        
    elif escolha_livro == "14":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7842741011/ref=zg_bs_nav_books_1") #Esportes
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "15":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7841775011/ref=zg_bs_nav_books_1") #Fantasia
        tentar()
        return redirect(url_for('livros'))

    elif escolha_livro == "16":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7872552011/ref=zg_bs_nav_books_1") #Gastronomia
        tentar()
        return redirect(url_for('livros'))

    elif escolha_livro == "17":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7842710011/ref=zg_bs_nav_books_1") #Hqs
        tentar()
        return redirect(url_for('livros'))

    elif escolha_livro == "18":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7843068011/ref=zg_bs_nav_books_1") #História
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "19":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7844001011/ref=zg_bs_nav_books_1") #Infantil
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "20":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7882627011/ref=zg_bs_nav_books_1")#Inglês e outras línguas
        tentar()
        return redirect(url_for('livros'))

    elif escolha_livro == "21":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7841279011/ref=zg_bs_nav_books_1") #Jovens 
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "22":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7843064011/ref=zg_bs_nav_books_1") #LGBTQIA
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "23":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7872687011/ref=zg_bs_nav_books_1")#Literatura 
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "24":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7874461011/ref=zg_bs_nav_books_1")#Medicina
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "25":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7872829011/ref=zg_bs_nav_books_1")#Policial
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "26":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7873971011/ref=zg_bs_nav_books_1")#política
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "27":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7874675011/ref=zg_bs_nav_books_1")#Religião
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "28":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7882388011/ref=zg_bs_nav_books_1")#Romance
        tentar()
        return redirect(url_for('livros'))
    
    elif escolha_livro == "29":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7882415011/ref=zg_bs_nav_books_1") #Saúde e Familia
        tentar()
        return redirect(url_for('livros'))

    elif escolha_livro == "30":
        navegador = webdriver.Chrome(service=server,options=chrome_options)
        navegador.get("https://www.amazon.com.br/gp/bestsellers/books/7882415011/ref=zg_bs_nav_books_1") #Turismo
        tentar()
        return redirect(url_for('livros'))
    else: 
        return render_template("index.html")

@app.route('/livros',methods=['GET','POST'])
def livros():
    global navegador


    tentar()
 
    #Caminhos dos dados a serem pegos
    titulo_pagina = navegador.find_element(By.CLASS_NAME,"a-size-large.a-spacing-medium.a-text-bold")
    titulo_distribuidor = navegador.find_elements(By.CLASS_NAME,"_cDEzb_p13n-sc-css-line-clamp-1_1Fn1y")
    preco = navegador.find_elements(By.CLASS_NAME,"_cDEzb_p13n-sc-price_3mJ9Z")
    imagens = navegador.find_elements(By.CLASS_NAME,"a-dynamic-image.p13n-sc-dynamic-image.p13n-product-image")

    #contadores
    separador = 0
     
    #Tabela para o Pandas
    tabela = {"Titulos":[],
              "Distribuidores":[],
              "Preços":[]}
    
    #Listas para enviar os dados para o HTML 
    lista_titulos = {"titulos":[]}
    lista_distribuidores = {"distribuidores":[]}
    lista_preco = {"preço":[]}
    lista_imagens = {"imagens":[]}

    #Separa titulo de distribuidor
    for i in titulo_distribuidor:
        if separador % 2 == 0:
            lista_titulos["titulos"].append(i.text)
            tabela["Titulos"].append(i.text)
            separador += 1
        else:
            lista_distribuidores["distribuidores"].append(i.text)
            tabela["Distribuidores"].append(i.text)
            separador += 1 
        if separador >= 20:
            break
        
    #Preços
    for i in preco:
        lista_preco["preço"].append(i.text)
        tabela["Preços"].append(i.text)
        if len(lista_preco["preço"]) >=10:
            break
    
    #Imagens
    for i in imagens:
        lista_imagens["imagens"].append(i.get_attribute("src"))
        if len(lista_imagens["imagens"]) >= 10:
            break
    
    
    criar_pasta()
    
    #Pega o titulo da pagina e transforma em texto para mostrar na seção de livros e cria outra variavél para utilizar nos nomes das tabelas
    titulo_pagina_text = titulo_pagina.text
    titulo_excel = titulo_pagina_text.replace(" ","_")
    df = pd.DataFrame(tabela)
    print(df)
    df.to_excel(f"./Arquivos/Dados_{titulo_excel}.xlsx",index=False)

    navegador.quit()


    
    return render_template('livros.html',
                           titulos = lista_titulos["titulos"],
                           distribuidores = lista_distribuidores["distribuidores"],
                           precos = lista_preco["preço"],
                           imagens = lista_imagens["imagens"],
                           titulo_pagina = titulo_pagina_text
                          )

@app.route('/sobre')
def sobre():
    return render_template("sobre.html")

app.run(debug=True)